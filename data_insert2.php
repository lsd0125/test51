<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_insert2';
?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        small.warning {
            color: red !important;
            display: none;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>
    <div id="info"></div>
<?php /*
    <?php if(isset($msg_code)):
        switch($msg_code){
            case 1:
                echo '<div class="alert alert-success" role="alert">
  資料新增完成
</div>';

                break;

            case -1:
                echo '<div class="alert alert-danger" role="alert">
  資料新增錯誤! 可能 email 重複!
</div>';

                break;
            default:
                echo '<div class="alert alert-danger" role="alert">
  資料新增錯誤!
</div>';

        }




        ?>


    <?php endif; ?>
*/ ?>
    <div class="col-md-6">
        <div class="card">

            <div class="card-body">
                <div class="card-title">新增資料</div>

                <form name="form1" method="post" action="" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="name">姓名</label>
                        <input type="text" class="form-control" name="name" id="name"
                               value=""
                               placeholder="請填入姓名">
                        <small id="nameWarning" class="form-text text-muted warning">請填寫正確的姓名</small>
                    </div>
                    <div class="form-group">
                        <label for="email">電郵</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value=""
                               placeholder="">
                        <small id="emailWarning" class="form-text text-muted warning">請填寫正確的電郵</small>

                    </div>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" name="mobile" id="mobile"
                               value=""
                               placeholder="">
                        <small id="mobileWarning" class="form-text text-muted warning">請輸入十位數的手機號碼</small>

                    </div>
                    <div class="form-group">
                        <label for="birthday">生日</label>
                        <input type="text" class="form-control datepicker" name="birthday" id="birthday"
                               value=""
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <input type="text" class="form-control" name="address" id="address"
                               value=""
                               placeholder="">
                    </div>

                    <button type="submit" class="btn btn-primary">新增</button>
                </form>

            </div>
        </div>



    </div>


</div>

    <script type="text/my-tpl" id="tpl1">
        <div class="alert alert-success" role="alert">
            資料新增完成
        </div>
    </script>
    <script type="text/my-tpl" id="tpl2">
        <div class="alert alert-danger" role="alert">
          資料新增錯誤! 可能 email 重複!
        </div>
    </script>
    <script type="text/my-tpl" id="tpl2">
        <div class="alert alert-danger" role="alert">
          資料新增錯誤!
        </div>
    </script>

    <script>
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });

        function checkForm() {
            $('small.warning').hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var name = document.form1.name.value;
            var email = document.form1.email.value;
            var mobile = document.form1.mobile.value;
            var isPass = true;

            mobile = mobile.trim();
            mobile = mobile.split('-').join('');
            if( name.length<2 ){
                $('#nameWarning').show();
                isPass = false;

            }
            if(! pattern.test(email) ){
                $('#emailWarning').show();
                isPass = false;

            }
            if(! /^09\d{8}$/.test(mobile) ){
                $('#mobileWarning').show();
                isPass = false;

            }

            if(isPass){
                $.post('data_insert2_backend.php', $(document.form1).serialize(), function(data){
                    console.log(data);
                    var info =$('#info');
                    info.empty();

                    switch (data['msg_code']){
                        case 1:
                            info.html($('#tpl1').text());
                            break;
                        case -1:
                            info.html($('#tpl2').text());
                            break;
                        default:
                            info.html($('#tpl3').text());
                            break;
                    }


                }, 'json');
            }


            return false;
        }



    </script>
<?php include __DIR__. '/__html_foot.php'; ?>