<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php

$ar1 = array(3,5,7,9);

$ar2 = [3,5,7,9];

$ar3 = array(
        'name' => 'peter',
        'age' => 24,
        'gender' => 'male',
);

$ar4 = [
    'name' => 'peter',
    'age' => 24,
    'gender' => 'male',
    123,
];



echo '<pre>';
print_r($ar3);
echo '</pre>';

echo '<br>';
echo '<pre>';
var_dump($ar4);
echo '</pre>';




?>



<script src="lib/jquery-3.2.1.min.js"></script>

</body>
</html>