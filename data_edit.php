<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_edit';

if(! isset($_GET['sid'])){
    header('Location: data_list.php');
    exit;
}
$sid = intval($_GET['sid']);
//
//$name = '';
//$email = '';
//$mobile = '';
//$birthday = '';
//$address = '';

if(isset($_POST['email'])){

    $name = strip_tags(trim($_POST['name']));
    $email = strip_tags(trim($_POST['email']));
    $mobile = strip_tags(trim($_POST['mobile']));
    $birthday = strip_tags(trim($_POST['birthday']));
    $address = strip_tags(trim($_POST['address']));

    // 後端檢查必填欄位
    // \[value\-\d{1,2}\]

    $sql = "UPDATE `address_book` SET 
            `name`=?,
            `email`=?,
            `mobile`=?,
            `birthday`=?,
            `address`=?
            WHERE `sid`=?";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssssi',
        $name,
        $email,
        $mobile,
        $birthday,
        $address,
        $sid
        );

    $stmt->execute();

    //echo "新增 {$stmt->affected_rows} 筆";

    $msg_code = $stmt->affected_rows;
}

$sql = "SELECT * FROM `address_book` WHERE `sid`=$sid ";
$result = $mysqli->query($sql);
$row = $result->fetch_assoc();
if(empty($row)){
    header('Location: data_list.php');
    exit;
}




?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        small.warning {
            color: red !important;
            display: none;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>


    <?php if(isset($msg_code)):
        switch($msg_code){
            case 1:
                echo '<div class="alert alert-success" role="alert">
  資料編輯完成
</div>';

                break;
            case -1:
                echo '<div class="alert alert-danger" role="alert">
  資料編輯錯誤! 可能 email 重複!
</div>';
                break;
            default:
                echo '<div class="alert alert-danger" role="alert">
  資料沒有變更!
</div>';

        }




        ?>


    <?php endif; ?>

    <div class="col-md-6">
        <div class="card">

            <div class="card-body">
                <div class="card-title">編輯資料</div>

                <form name="form1" method="post" action="" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="name">姓名</label>
                        <input type="text" class="form-control" name="name" id="name"
                               value="<?= $row['name'] ?>"
                               placeholder="請填入姓名">
                        <small id="nameWarning" class="form-text text-muted warning">請填寫正確的姓名</small>
                    </div>
                    <div class="form-group">
                        <label for="email">電郵</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="<?= $row['email'] ?>"
                               placeholder="">
                        <small id="emailWarning" class="form-text text-muted warning">請填寫正確的電郵</small>

                    </div>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" name="mobile" id="mobile"
                               value="<?= $row['mobile'] ?>"
                               placeholder="">
                        <small id="mobileWarning" class="form-text text-muted warning">請輸入十位數的手機號碼</small>

                    </div>
                    <div class="form-group">
                        <label for="birthday">生日</label>
                        <input type="text" class="form-control datepicker" name="birthday" id="birthday"
                               value="<?= $row['birthday'] ?>"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <input type="text" class="form-control" name="address" id="address"
                               value="<?= $row['address'] ?>"
                               placeholder="">
                    </div>

                    <button type="submit" class="btn btn-primary">編輯</button>
                </form>

            </div>
        </div>



    </div>


</div>
    <script>
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });

        function checkForm() {
            $('small.warning').hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var name = document.form1.name.value;
            var email = document.form1.email.value;
            var mobile = document.form1.mobile.value;
            var isPass = true;

            mobile = mobile.trim();
            mobile = mobile.split('-').join('');
            if( name.length<2 ){
                $('#nameWarning').show();
                isPass = false;

            }
            if(! pattern.test(email) ){
                $('#emailWarning').show();
                isPass = false;

            }
            if(! /^09\d{8}$/.test(mobile) ){
                $('#mobileWarning').show();
                isPass = false;

            }



            return isPass;
        }

/*

/[A-Z]{2}\d{8}/
/09\d{8}/
/^09\d{8}$/

 */

    </script>
<?php include __DIR__. '/__html_foot.php'; ?>