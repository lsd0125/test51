<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav nav-pills mr-auto">
            <li class="nav-item">
                <a class="nav-link <?= $page_name=='data_list' ? 'active' : '' ?>" href="data_list.php">資料列表</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $page_name=='data_insert' ? 'active' : '' ?>" href="data_insert.php">新增資料</a>
            </li>
            <li class="nav-item">
                <a class="nav-link <?= $page_name=='data_insert2' ? 'active' : '' ?>" href="data_insert2.php">新增資料(AJAX)</a>
            </li>
        </ul>
    </div>
</nav>