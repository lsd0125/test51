<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_list';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1;

$per_page = 5;

$t_result = $mysqli->query("SELECT COUNT(1) FROM address_book");

$total_rows = $t_result->fetch_row()[0];

$total_pages = ceil($total_rows/$per_page);
/*
$page = $page<1 ? 1 : $page;
$page = $page>$total_pages ? $total_pages : $page;
*/

if($page<1){
    $page = 1;
}
if($page>$total_pages){
    $page = $total_pages;
}


$c_sql = sprintf("SELECT * FROM `address_book` ORDER BY `sid` DESC LIMIT %s, %s", ($page-1)*$per_page, $per_page);
$c_result = $mysqli->query($c_sql);

?>
<?php include __DIR__. '/__html_head.php'; ?>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>

    <div>
        <nav aria-label="">
            <ul class="pagination">
                <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=1" tabindex="-1">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $page<=1 ? 1 : $page-1 ?>" tabindex="-1">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1"><?=  $page. ' / '. $total_pages  ?></a>
                </li>
                <li class="page-item <?= $page>=$total_pages ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $page>=$total_pages ? $total_pages : $page+1 ?>" tabindex="-1">
                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="page-item <?= $page>=$total_pages ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $total_pages ?>" tabindex="-1">
                        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>


    <div class="col-md-12">


        <table class="table table-striped table-dark">

            <thead>
            <tr>
                <th>編輯</th>
                <th>編號</th>
                <th>姓名</th>
                <th>電郵</th>
                <th>手機</th>
                <th>生日</th>
                <th>地址</th>
                <th>刪除</th>
            </tr>
            </thead>
            <tbody>
            <?php while ($row = $c_result->fetch_assoc()) { ?>
                <tr>
                    <td><a href="data_edit.php?sid=<?= $row['sid'] ?>"><i class="fas fa-edit"></i></a></td>
                    <td><?= $row['sid'] ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><?= $row['mobile'] ?></td>
                    <td><?= $row['birthday'] ?></td>
                    <td><?= htmlentities($row['address']) ?></td>
                    <td><a href="javascript: delete_it(<?= $row['sid'] ?>)"><i class="fas fa-trash-alt"></i></a></td>

                </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>


    <div>
        <nav aria-label="">
            <ul class="pagination pagination-sm">
                <li class="page-item <?= $page<=1 ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=1" tabindex="-1">
                        <i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
                    </a>
                </li>

                <?php for($i=1; $i<=$total_pages; $i++): ?>
                <li class="page-item <?= $i==$page ? 'active' : '' ?>">
                    <a class="page-link" <?= $i==$page ? '' : "href=\"?page={$i}\" " ?> >
                       <?= $i ?>
                    </a>
                </li>
                <?php endfor; ?>

                <li class="page-item <?= $page>=$total_pages ? 'disabled' : '' ?>">
                    <a class="page-link" href="?page=<?= $total_pages ?>" tabindex="-1">
                        <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
    <script>
        function delete_it(sid){
            if(confirm('你確定要刪除編號為 ' + sid + ' 的資料嗎?')){
                location.href = "data_delete.php?sid=" +sid;
            }
        }
    </script>
<?php include __DIR__. '/__html_foot.php'; ?>