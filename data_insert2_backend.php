<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_insert';

$payload = [
  'success' => false,
  'info' => '沒有送出的表單資料!',
];

if(isset($_POST['email'])){
    // strip_tags()
    $name = strip_tags(trim($_POST['name']));
    $email = strip_tags(trim($_POST['email']));
    $mobile = strip_tags(trim($_POST['mobile']));
    $birthday = strip_tags(trim($_POST['birthday']));
    $address = strip_tags(trim($_POST['address']));

    // 後端檢查必填欄位

    $sql = "INSERT INTO `address_book`(
      `name`, `email`, `mobile`,
      `birthday`, `address`, `created_at`
    ) VALUES (
      ?, ?, ?,
      ?, ?, NOW()
    )";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssss',
        $name,
        $email,
        $mobile,
        $birthday,
        $address
        );

    $stmt->execute();

    //echo "新增 {$stmt->affected_rows} 筆";

    $msg_code = $stmt->affected_rows;
    $payload['msg_code'] = $msg_code;
    if($msg_code==1){
        $payload['success'] = true;
        $payload['info'] = '';
    }


}

echo json_encode($payload, JSON_UNESCAPED_UNICODE);


