<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php
    $a=10;
    $b=20;


    function swap(&$m, &$n){
        $t = $m;
        $m = $n;
        $n = $t;
    }


    swap($a, $b);

    echo "$a, $b";
?>



<script src="lib/jquery-3.2.1.min.js"></script>
<script>
    var a=10, b=20;

    var t;

/*
    t = a;
    a = b;
    b = t;

    console.log(a, b);

*/
    function swap(m, n){
        var t = m;
        m = n;
        n = t;
    }


    swap(a, b);

    console.log(a, b);



</script>
</body>
</html>