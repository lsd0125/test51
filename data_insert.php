<?php
require __DIR__ . '/__db_connect.php';
$page_name = 'data_insert';

$name = '';
$email = '';
$mobile = '';
$birthday = '';
$address = '';

if(isset($_POST['email'])){
    // strip_tags()
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $mobile = trim($_POST['mobile']);
    $birthday = trim($_POST['birthday']);
    $address = trim($_POST['address']);

    // 後端檢查必填欄位

    $sql = "INSERT INTO `address_book`(
      `name`, `email`, `mobile`,
      `birthday`, `address`, `created_at`
    ) VALUES (
      ?, ?, ?,
      ?, ?, NOW()
    )";

    $stmt = $mysqli->prepare($sql);

    $stmt->bind_param('sssss',
        $_POST['name'],
        $_POST['email'],
        $_POST['mobile'],
        $_POST['birthday'],
        $_POST['address']
        );

    $stmt->execute();

    //echo "新增 {$stmt->affected_rows} 筆";

    $msg_code = $stmt->affected_rows;

    /*
    $sql = sprintf("INSERT INTO `address_book`(
      `name`, `email`, `mobile`,
      `birthday`, `address`, `created_at`
    ) VALUES (
      '%s', '%s', '%s',
      '%s', '%s', NOW()
    )",
            $mysqli->escape_string($_POST['name']),
            $mysqli->escape_string($_POST['email']),
            $mysqli->escape_string($_POST['mobile']),
            $mysqli->escape_string($_POST['birthday']),
            $mysqli->escape_string($_POST['address'])
    );

    $mysqli->query($sql);
    echo $sql;
    die('<br>ok');
    //    echo 'OK';
    //    exit;

*/

}


?>
<?php include __DIR__. '/__html_head.php'; ?>
    <style>
        small.warning {
            color: red !important;
            display: none;
        }
    </style>
<div class="container">
    <?php include __DIR__. '/__navbar.php'; ?>


    <?php if(isset($msg_code)):
        switch($msg_code){
            case 1:
                echo '<div class="alert alert-success" role="alert">
  資料新增完成
</div>';

                break;

            case -1:
                echo '<div class="alert alert-danger" role="alert">
  資料新增錯誤! 可能 email 重複!
</div>';

                break;
            default:
                echo '<div class="alert alert-danger" role="alert">
  資料新增錯誤!
</div>';

        }




        ?>


    <?php endif; ?>

    <div class="col-md-6">
        <div class="card">

            <div class="card-body">
                <div class="card-title">新增資料</div>

                <form name="form1" method="post" action="" onsubmit="return checkForm()">
                    <div class="form-group">
                        <label for="name">姓名</label>
                        <input type="text" class="form-control" name="name" id="name"
                               value="<?= $name ?>"
                               placeholder="請填入姓名">
                        <small id="nameWarning" class="form-text text-muted warning">請填寫正確的姓名</small>
                    </div>
                    <div class="form-group">
                        <label for="email">電郵</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="<?= $email ?>"
                               placeholder="">
                        <small id="emailWarning" class="form-text text-muted warning">請填寫正確的電郵</small>

                    </div>
                    <div class="form-group">
                        <label for="mobile">手機</label>
                        <input type="text" class="form-control" name="mobile" id="mobile"
                               value="<?= $mobile ?>"
                               placeholder="">
                        <small id="mobileWarning" class="form-text text-muted warning">請輸入十位數的手機號碼</small>

                    </div>
                    <div class="form-group">
                        <label for="birthday">生日</label>
                        <input type="text" class="form-control datepicker" name="birthday" id="birthday"
                               value="<?= $birthday ?>"
                               placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="address">地址</label>
                        <input type="text" class="form-control" name="address" id="address"
                               value="<?= $address ?>"
                               placeholder="">
                    </div>

                    <button type="submit" class="btn btn-primary">新增</button>
                </form>

            </div>
        </div>



    </div>


</div>
    <script>
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });

        function checkForm() {
            $('small.warning').hide();

            var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var name = document.form1.name.value;
            var email = document.form1.email.value;
            var mobile = document.form1.mobile.value;
            var isPass = true;

            mobile = mobile.trim();
            mobile = mobile.split('-').join('');
            if( name.length<2 ){
                $('#nameWarning').show();
                isPass = false;

            }
            if(! pattern.test(email) ){
                $('#emailWarning').show();
                isPass = false;

            }
            if(! /^09\d{8}$/.test(mobile) ){
                $('#mobileWarning').show();
                isPass = false;

            }



            return isPass;
        }

/*

/[A-Z]{2}\d{8}/
/09\d{8}/
/^09\d{8}$/

 */

    </script>
<?php include __DIR__. '/__html_foot.php'; ?>